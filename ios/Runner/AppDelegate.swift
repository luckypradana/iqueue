import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  private var methodChannel: FlutterMethodChannel?

    override func application(
      _ application: UIApplication,
      didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
    ) -> Bool {

      let controller = window.rootViewController as! FlutterViewController
      methodChannel = FlutterMethodChannel(name: "54.254.142.85:12001/user/activateAccount", binaryMessenger: controller)

      methodChannel?.setMethodCallHandler({ (call: FlutterMethodCall, result: FlutterResult) in
        guard call.method == "initialLink" else {
          result(FlutterMethodNotImplemented)
          return
        }
      })


      GeneratedPluginRegistrant.register(with: self)
      return super.application(application, didFinishLaunchingWithOptions: launchOptions)
}
