import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:iqueue/config/constant.dart';
import 'package:iqueue/config/navigation.dart';
import 'package:iqueue/page/admin_home_page.dart';
import 'package:iqueue/page/create_queue_page.dart';
import 'package:iqueue/page/create_service_page.dart';
import 'package:iqueue/page/forgot_password_page.dart';
import 'package:iqueue/page/home_page.dart';
import 'package:iqueue/page/landing_page.dart';
import 'package:iqueue/page/profile_page.dart';
import 'package:iqueue/page/queues_page.dart';
import 'package:iqueue/page/register_page.dart';
import 'package:iqueue/page/splash_page.dart';
import 'package:redux/redux.dart';

import 'appstate/app_state.dart';
import 'appstate/reducers.dart';
import 'locale/app_localizations.dart';

void main() {
  final _initialState = AppState();
  final Store<AppState> _store =
  Store<AppState>(reducer, initialState: _initialState);
  runApp(MyApp(store: _store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Constant.cPrimaryColor,
          accentColor: Colors.grey[400],
          buttonTheme: ButtonThemeData(
            buttonColor: Constant.cPrimaryColor,
            textTheme: ButtonTextTheme.primary,
          ),
        ),
        supportedLocales: [
          Locale('id', ''),
        ],
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        initialRoute: Navigation.SPLASH,
        routes: {
          Navigation.SPLASH: (context) => SplashPage(),
          Navigation.LANDING: (context) => LandingPage(),
          Navigation.FORGOT_PASSWORD: (context) => ForgotPasswordPage(),
          Navigation.REGISTER: (context) => RegisterPage(),
          Navigation.HOME: (context) => HomePage(),
          Navigation.PROFILE: (context) => ProfilePage(),
          Navigation.CREATE_QUEUE: (context) => CreateQueuePage(),
          Navigation.CREATE_SERVICE: (context) => CreateServicePage(),
          Navigation.VIEW_QUEUE: (context) => QueuesPage(),
          Navigation.ADMIN_HOME: (context) => AdminHomePage(),
        },
      ),
    );
  }
}
