import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show PlatformException;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:iqueue/appstate/actions.dart';
import 'package:iqueue/appstate/app_state.dart';
import 'package:iqueue/config/pref.dart';
import 'package:iqueue/data/user.dart';
import 'package:iqueue/data/user_response.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';

import 'base_service.dart';

class UserService extends BaseService {
  Future<UserResponse> login(
      BuildContext context, String email, String password) async {
    Map data = {
      'email': email,
      'password': password,
    };
    final response = await post('sso/logon', data);
    var body = UserResponse.fromJson(json.decode(response.body));

    if (body.respCode == '00') {
      final prefs = await SharedPreferences.getInstance();
      var session = response.headers['set-cookie'].split(" ")[0];
      prefs.setString(Pref.USER, jsonEncode(body.data));
      prefs.setString(Pref.SESSION, session);
      StoreProvider.of<AppState>(context).dispatch(SetUser(body.data));
      return body;
    } else
      throw Exception(body.respMsg);
  }

  Future<User> getUser(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    var response = prefs.getString(Pref.USER) ?? null;
    if (response != null) {
      try {
        var body = User.fromJson(json.decode(response));
        StoreProvider.of<AppState>(context).dispatch(SetUser(body));
        return body;
      } on Exception catch (e) {
        throw e;
      }
    } else
      return null;
  }

  void logout(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(Pref.USER, null);
    StoreProvider.of<AppState>(context).dispatch(SetUser(null));
  }

  register(BuildContext context, String email, String password, String name,
      String address, String phone) async {
    Map data = {
      'email': email,
      'password': password,
      'fullName': name,
      'address': address,
      'phoneNumber': phone,
    };
    final response = await post('user/save', data);
    var body = UserResponse.fromJson(json.decode(response.body));

    if (body.respCode == '00')
      return body;
    else
      throw Exception(body.respMsg);
  }

  Future<bool> handleActivationAccount(BuildContext context) async {
    try {
      var initialLink = await getInitialLink();
      if (initialLink != null) return initialLink.contains("activateAccount");
      return false;
    } on PlatformException {
      return false;
    }
  }
}
