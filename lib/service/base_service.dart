import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:iqueue/config/constant.dart';

class BaseService {
  Future<http.Response> post(endpoint, data) async {
    try {
      var response = await http
          .post(
            Constant.URL + endpoint,
            headers: {"Content-Type": "application/json"},
            body: json.encode(data),
          )
          .timeout(const Duration(seconds: 10));
      return response;
    } on TimeoutException {
      throw Exception("Gagal mengirim data, harap coba lagi");
    } on SocketException {
      throw Exception("Gagal mengirim data, harap coba lagi");
    }
  }
}
