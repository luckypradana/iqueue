import 'actions.dart';
import 'app_state.dart';

AppState reducer(AppState prevState, dynamic action) {
  AppState newState = AppState.fromAppState(prevState);
  if (action is SetUser) newState.user = action.payload;
  if (action is SetActivatedAccount)
    newState.activatedAccountLink = action.link;
  return newState;
}
