import 'package:iqueue/data/user.dart';

class AppState {
  User user;
  String activatedAccountLink;

  AppState({this.user});

//  AppState(
//      {@required this.sliderFontSize, this.bold = false, this.italic = false});

  AppState.fromAppState(AppState another) {
    user = another.user;
  }

  User get getUser => user;
}
