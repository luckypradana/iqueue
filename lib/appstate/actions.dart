import 'package:iqueue/data/user.dart';

/// Actions with Payload
class SetUser {
  final User payload;

  SetUser(this.payload);
}

class SetActivatedAccount {
  final String link;

  SetActivatedAccount(this.link);
}
