import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  final String email;
  final String password;
  final String fullName;
  final String roleId;

  User({this.email, this.password, this.fullName, this.roleId});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  static const String ROLE_ADMIN = 'ADM';
  static const String ROLE_USER = 'USR';
}
