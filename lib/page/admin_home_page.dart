import 'package:flutter/material.dart';
import 'package:iqueue/subpage/admin_home_list_page.dart';
import 'package:iqueue/subpage/admin_home_customer_page.dart';
import 'package:iqueue/subpage/admin_home_staff_schedule_page.dart';
import 'package:iqueue/subpage/admin_home_transaction_page.dart';

class AdminHomePage extends StatefulWidget {
  @override
  _AdminHomePageState createState() => _AdminHomePageState();
}

class _AdminHomePageState extends State<AdminHomePage> {
  int _selectedIndex = 0;
  final List<Widget> _subpage = [
    AdminHomeQueueSubpage(),
    AdminHomeTransactionSubpage(),
    AdminHomeStaffScheduleSubpage(),
    AdminHomeCustomerSubpage()
  ];

  void onTabTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("iQueue"),
        centerTitle: true,
        elevation: 0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.person),
            onPressed: (){},
          )
        ],
        automaticallyImplyLeading: false,
      ),
      body: _subpage[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: onTabTapped,
        elevation: 0,
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Beranda"),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.attach_money),
            title: Text("Transaksi"),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            title: Text("Jadwal Staff"),
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.group),
            title: Text("Customer"),
          ),
        ],
      ),
    );
  }
}
