import 'package:flutter/material.dart';
import 'package:iqueue/service/user_service.dart';

class ProfilePage extends StatelessWidget {
  var service = UserService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Profil"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: Text('Logout'),
                onPressed: () {
                  service.logout(context);
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/landing', (Route<dynamic> route) => false);
                },
              ),
            ],
          ),
        ));
  }
}
