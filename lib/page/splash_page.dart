import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:iqueue/config/constant.dart';
import 'package:iqueue/config/navigation.dart';
import 'package:iqueue/data/user.dart';
import 'package:iqueue/locale/app_localizations.dart';
import 'package:iqueue/page/base_page.dart';
import 'package:iqueue/service/user_service.dart';

class SplashPage extends BasePage {
  @override
  Widget build(BuildContext context) {
    return SplashWidget();
  }
}

// Create a Form widget.
class SplashWidget extends StatefulWidget {
  @override
  SplashWidgetState createState() {
    return SplashWidgetState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class SplashWidgetState extends State<SplashWidget> {
  final service = UserService();

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () async {
      await service.getUser(context)
          .then((value) => redirect(value))
          .catchError((e) {
//        Common.showAlertDialog(context, e);
        redirect(null);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              AppLocalizations.of(context).translate('app_name'),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 48,
              ),
            ),
            SizedBox(height: 20.0),
            SpinKitRing(
              color: Constant.cPrimaryColor,
              size: 40.0,
            )
          ],
        ));
  }

  redirect(User user) {
    var navigation = Navigation.LANDING;
    if (user != null) navigation =
    user.roleId == User.ROLE_ADMIN ? Navigation.ADMIN_HOME : Navigation.HOME;

    Navigator.pushNamedAndRemoveUntil(
        context, navigation, (Route<dynamic> route) => false);
  }
}
