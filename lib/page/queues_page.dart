import 'package:flutter/material.dart';

class QueuesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Queue"),
        ),
        body: Padding(
          padding: EdgeInsets.all(64.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ListView.builder(
                itemBuilder: _buildProductItem,
                itemCount: 5,
              ),
              RaisedButton(
                child: Text('Start Queue'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ));
  }

  Widget _buildProductItem(BuildContext context, int index) {
    return Text(index.toString());
  }
}
