import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:iqueue/appstate/app_state.dart';
import 'package:iqueue/data/user.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var role = 'admin';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("iQueue"),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, '/profile');
                  },
                  child: Icon(
                    Icons.settings,
                  ),
                )),
          ],
        ),
        body: Center(
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: StoreConnector<AppState, AppState>(
                converter: (store) => store.state,
                builder: (context, state) {
                  return _buildMenu(context, state.user);
                }
            ),
          ),
        ));
  }

  _buildMenu(BuildContext context, User user) {
    if (user.roleId == User.ROLE_ADMIN) {
      return Container(
        child: RaisedButton(
          child: Text('Create Service'),
          onPressed: () {
            Navigator.pushNamed(context, '/createservice');
          },
        ),
      );
    } else {
      return Container(
        child: RaisedButton(
          child: Text('Start Queue'),
          onPressed: () {
            Navigator.pushNamed(context, '/createqueue');
          },
        ),
      );
    }
  }
}
