import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iqueue/data/user.dart';
import 'package:iqueue/data/user_response.dart';
import 'package:iqueue/helper/common.dart';
import 'package:iqueue/locale/app_localizations.dart';
import 'package:iqueue/page/base_page.dart';
import 'package:iqueue/service/user_service.dart';
import 'package:iqueue/validator/input_length.dart';
import 'package:iqueue/validator/validator.dart';

class RegisterPage extends BasePage {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Container(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(72.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).translate('app_name'),
                      style: TextStyle(
                          fontSize: 50, fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Text(
                        AppLocalizations.of(context).translate('register'),
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                    RegisterForm(),
                    Container(
                        margin: EdgeInsets.only(top: 24.0),
                        width: double.infinity,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: GestureDetector(
                                child: Text(
                                    AppLocalizations.of(context).translate(
                                        'back'),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.blue)),
                                onTap: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                          ],
                        ))
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

// Create a Form widget.
class RegisterForm extends StatefulWidget {
  @override
  RegisterFormState createState() {
    return RegisterFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();
  final service = UserService();
  String _password;
  String _email;
  String _name;
  String _address;
  String _phone;
  Future<User> futureLogin;

  @override
  Widget build(BuildContext context) {

    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: TextFormField(
              onSaved: (value) => _email = value,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (term) {
                FocusScope.of(context).nextFocus();
              },
              inputFormatters: [
                LengthLimitingTextInputFormatter(InputLength.maxEmail)
              ],
              validator: (value) {
                return Validator.validateEmail(value);
              },
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                errorMaxLines: 2,
                enabledBorder: UnderlineInputBorder(),
                hintText: AppLocalizations.of(context).translate('email'),
              ),
              keyboardType: TextInputType.emailAddress,
            ),
          ),
          TextFormField(
            onSaved: (value) => _password = value,
            obscureText: true,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (term) {
              FocusScope.of(context).nextFocus();
            },
            keyboardType: TextInputType.visiblePassword,
            inputFormatters: [
              LengthLimitingTextInputFormatter(InputLength.maxPassword)
            ],
            validator: (value) {
              return Validator.validatePassword(value);
            },
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              errorMaxLines: 2,
              enabledBorder: UnderlineInputBorder(),
              hintText: AppLocalizations.of(context).translate('password'),
            ),
          ),
          TextFormField(
            onSaved: (value) => _name = value,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (term) {
              FocusScope.of(context).nextFocus();
            },
            inputFormatters: [
              LengthLimitingTextInputFormatter(InputLength.maxName)
            ],
            validator: (value) {
              return Validator.validateName(value);
            },
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              errorMaxLines: 2,
              enabledBorder: UnderlineInputBorder(),
              hintText: AppLocalizations.of(context).translate('name'),
            ),
          ),
          TextFormField(
            onSaved: (value) => _address = value,
            keyboardType: TextInputType.multiline,
            inputFormatters: [
              LengthLimitingTextInputFormatter(InputLength.maxAddress)
            ],
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              errorMaxLines: 2,
              enabledBorder: UnderlineInputBorder(),
              hintText: AppLocalizations.of(context).translate('address'),
            ),
          ),
          TextFormField(
            onSaved: (value) => _phone = value,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.done,
            onFieldSubmitted: (term) {
              FocusScope.of(context).unfocus();
            },
            validator: (value) {
              return Validator.validatePhone(value);
            },
            inputFormatters: [
              LengthLimitingTextInputFormatter(InputLength.maxPhone)
            ],
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              errorMaxLines: 2,
              enabledBorder: UnderlineInputBorder(),
              hintText: AppLocalizations.of(context).translate('phone_number'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: SizedBox(
              width: double.infinity,
              child: RaisedButton(
                child: Text(
                  AppLocalizations.of(context)
                      .translate('action_register')
                      .toUpperCase(),
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () async {
                  // save the fields..
                  final form = _formKey.currentState;
                  form.save();
                  if (_formKey.currentState.validate()) {
                    FocusScope.of(context).requestFocus(FocusNode());
                    Common.showProgressDialog(context);
                    service
                        .register(
                        context, _email, _password, _name, _address, _phone)
                        .then((response) => processRegister(response))
                        .catchError((e) async {
                      Navigator.of(context).pop();
                      Common.showSnackbar(context, e);
                    });
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  processRegister(UserResponse response) {
    if (response.respCode != "00") {
      Navigator.of(context).pop();
      Common.showAlertDialog(context, response.respMsg, () {
        Navigator.of(context).pop();
      });
    } else
      Common.showAlertDialog(context, response.respMsg, () {
        Navigator.pushNamedAndRemoveUntil(
            context, "/landing", (Route<dynamic> route) => false);
      });
  }
}
