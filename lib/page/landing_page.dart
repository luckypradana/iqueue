import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iqueue/config/navigation.dart';
import 'package:iqueue/data/user.dart';
import 'package:iqueue/data/user_response.dart';
import 'package:iqueue/helper/common.dart';
import 'package:iqueue/locale/app_localizations.dart';
import 'package:iqueue/page/base_page.dart';
import 'package:iqueue/service/user_service.dart';
import 'package:iqueue/validator/input_length.dart';
import 'package:iqueue/validator/validator.dart';

class LandingPage extends BasePage {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Container(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(72.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).translate('app_name'),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 48,
                      ),
                    ),
                    LoginForm(),
                    Container(
                        margin: EdgeInsets.only(top: 24.0),
                        width: double.infinity,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: GestureDetector(
                                child: Text(
                                    AppLocalizations.of(context)
                                        .translate('forgot_password'),
                                    style: TextStyle(color: Colors.blue)),
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, '/forgotpassword');
                                },
                              ),
                            ),
                            GestureDetector(
                              child: Text(
                                  AppLocalizations.of(context)
                                  .translate('register'),
                                  style: TextStyle(color: Colors.blue)),
                              onTap: () {
                                Navigator.pushNamed(context, '/register');
                              },
                            ),
                          ],
                        ))
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

// Create a Form widget.
class LoginForm extends StatefulWidget {
  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final service = UserService();
  String _password;
  String _email;
  Future<User> futureLogin;

  @override
  Widget build(BuildContext context) {
    _handleActivationAccount();

    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 56.0),
            child: TextFormField(
              key: Key("email"),
              onSaved: (value) => _email = value,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (term) {
                FocusScope.of(context).nextFocus();
              },
              inputFormatters: [
                LengthLimitingTextInputFormatter(InputLength.maxEmail)
              ],
              validator: (value) {
                return Validator.validateEmail(value);
              },
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                errorMaxLines: 2,
                enabledBorder: UnderlineInputBorder(),
                hintText: AppLocalizations.of(context).translate('email'),
              ),
            ),
          ),
          TextFormField(
            key: Key("password"),
            onSaved: (value) => _password = value,
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
            textInputAction: TextInputAction.done,
            onFieldSubmitted: (term) {
              FocusScope.of(context).unfocus();
            },
            inputFormatters: [
              LengthLimitingTextInputFormatter(InputLength.maxPassword)
            ],
            validator: (value) {
              return Validator.validatePassword(value);
            },
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              errorMaxLines: 2,
              enabledBorder: UnderlineInputBorder(),
              hintText: AppLocalizations.of(context).translate('password'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: SizedBox(
              width: double.infinity,
              child: RaisedButton(
                key: Key("login"),
                child: Text(
                  AppLocalizations.of(context).translate('login').toUpperCase(),
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () async {
                  // save the fields..
                  final form = _formKey.currentState;
                  form.save();
                  if (_formKey.currentState.validate()) {
                    FocusScope.of(context).requestFocus(FocusNode());
                    Common.showProgressDialog(context);
                    service
                        .login(context, _email, _password)
                        .then((response) => processLogin(response))
                        .catchError((e) async {
                      Navigator.of(context).pop();
                      Common.showExceptionAlertDialog(context, e);
                    });
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  processLogin(UserResponse response) {
    Navigator.pushNamedAndRemoveUntil(
        context,
        response.data.roleId == User.ROLE_ADMIN
            ? Navigation.ADMIN_HOME
            : Navigation.HOME,
            (Route<dynamic> route) => false);
  }

  void _handleActivationAccount() async {
    if (await service.handleActivationAccount(context))
      Common.showAlertDialog(context, "Akun Anda berhasil diaktivasi");
  }
}
