import 'package:flutter/material.dart';

class AdminHomeCustomerSubpage extends StatefulWidget {
  @override
  _AdminHomeCustomerState createState() => _AdminHomeCustomerState();
}

class _AdminHomeCustomerState extends State<AdminHomeCustomerSubpage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "AdminHomeCustomerSubpage",
        style: TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.bold
        ),
      ),
    );
  }
}
