import 'package:flutter/material.dart';

class Queue {
  String queueNo;
  String custName;
  String serviceType;
  String status;

  Queue({this.queueNo, this.custName, this.serviceType, this.status});
}

class AdminHomeQueueSubpage extends StatefulWidget {
  @override
  _AdminHomeQueueListState createState() => _AdminHomeQueueListState();
}

class _AdminHomeQueueListState extends State<AdminHomeQueueSubpage> {
  final List<Queue> queues = [
    Queue(queueNo: "Q1", custName: "John Due", serviceType: "Ringan", status: "Menunggu"),
    Queue(queueNo: "Q2", custName: "James Bond", serviceType: "Berat", status: "Selesai"),
    Queue(queueNo: "Q1", custName: "Son Goku", serviceType: "Ringan", status: "Menunggu"),
    Queue(queueNo: "Q1", custName: "Son Gohan", serviceType: "Ringan", status: "Menunggu"),
    Queue(queueNo: "Q1", custName: "Krilin", serviceType: "Ringan", status: "Menunggu"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              "Antrian Customer",
              style: Theme.of(context).textTheme.display2,
            ),
          ),
          Expanded(
            child: ListView.builder(
                itemCount: queues.length,
                itemBuilder: (context, index){
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 5.0),
                    child: Card(
                      child: ListTile(
                        onTap: (){},
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(flex: 1, child: Text(queues[index].queueNo)),
                            Expanded(flex: 3, child: Text(queues[index].custName)),
                            Expanded(flex: 2, child: Text(queues[index].serviceType)),
                            Expanded(flex: 2, child: Text(queues[index].status)),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
