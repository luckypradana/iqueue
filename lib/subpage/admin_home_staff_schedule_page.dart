import 'package:flutter/material.dart';

class AdminHomeStaffScheduleSubpage extends StatefulWidget {
  @override
  _AdminHomeStaffScheduleSubpageState createState() => _AdminHomeStaffScheduleSubpageState();
}

class _AdminHomeStaffScheduleSubpageState extends State<AdminHomeStaffScheduleSubpage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "AdminHomeStaffScheduleSubpage",
        style: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold
        ),
      ),
    );
  }
}
