import 'package:flutter/material.dart';

class AdminHomeTransactionSubpage extends StatefulWidget {
  @override
  _AdminHomeTransactionSubpageState createState() => _AdminHomeTransactionSubpageState();
}

class _AdminHomeTransactionSubpageState extends State<AdminHomeTransactionSubpage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "AdminHomeTransactionSubpage",
        style: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold
        ),
      ),
    );
  }
}
