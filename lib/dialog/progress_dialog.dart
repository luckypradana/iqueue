import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:iqueue/config/constant.dart';

class ProgressDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(),
        child: Center(
          child: Container(
            decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: new BorderRadius.all(Radius.circular(4.0))),
            width: 72,
            height: 72,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: SpinKitRing(
                color: Constant.cPrimaryColor,
                size: 40.0,
              ),
            ),
          ),
        ));
  }
}
