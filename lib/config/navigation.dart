class Navigation {
  static const String SPLASH = '/';
  static const String LANDING = '/landing';
  static const String FORGOT_PASSWORD = '/forgotpassword';
  static const String REGISTER = '/register';
  static const String HOME = '/home';
  static const String PROFILE = '/profile';
  static const String CREATE_QUEUE = '/createqueue';
  static const String CREATE_SERVICE = '/createservice';
  static const String VIEW_QUEUE = '/viewqueue';
  static const String ADMIN_HOME = '/adminhome';
}
