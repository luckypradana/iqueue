import 'package:flutter/material.dart';
import 'package:iqueue/dialog/progress_dialog.dart';

class Common {
  static showSnackbar(BuildContext context, Exception exception) {
    final snackBar = SnackBar(content: Text(getExceptionMessage(exception)));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  static String getExceptionMessage(Exception e) {
    return e.toString().replaceAll("Exception: ", "");
  }

  static Future<void> showExceptionAlertDialog(BuildContext context,
      Exception exception,
      [Function onDoneClicked]) async {
    return showAlertDialog(
        context, getExceptionMessage(exception), onDoneClicked);
  }

  static Future<void> showAlertDialog(BuildContext context, String text,
      [Function onDoneClicked]) async {
    if (onDoneClicked == null)
      onDoneClicked = () {
        Navigator.of(context).pop();
      };

    return await showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(text),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: onDoneClicked,
            ),
          ],
        );
      },
    );
  }

  static void showProgressDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return ProgressDialog();
        });
  }
}
