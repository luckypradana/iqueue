class InputLength {
  static final int maxEmail = 35;
  static final int maxName = 32;
  static final int maxAddress = 100;
  static final int maxPassword = 32;
  static final int maxPhone = 13;

  static final int minName = 3;
  static final int minPassword = 6;
  static final int minPhone = 10;
}
