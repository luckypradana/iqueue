import 'package:iqueue/validator/input_length.dart';

class Validator {
  static String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (value.isEmpty) return 'Harap masukkan email';
    if (!regex.hasMatch(value)) return 'Harap masukkan email dengan benar';
    return null;
  }

  static String validatePassword(String value) {
    if (value.isEmpty) return 'Harap masukkan password';
    if (value.length < InputLength.minPassword)
      return 'Harap masukkan password dengan minimal ' +
          InputLength.minPassword.toString() +
          ' karakter';
    return null;
  }

  static String validateName(String value) {
    if (value.isEmpty) return 'Harap masukkan nama';
    if (value.length < InputLength.minName)
      return 'Harap masukkan nama dengan minimal ' +
          InputLength.minName.toString() +
          ' karakter';
    return null;
  }

  static String validatePhone(String value) {
    if (value.isEmpty) return 'Harap masukkan nomor handphone';
    if (value.length < InputLength.minPhone)
      return 'Harap masukkan nomor handphone dengan minimal ' +
          InputLength.minPhone.toString() +
          ' karakter';
    if (!value.startsWith("0"))
      return 'Nomor handphone harus dimulai dengan angka 0';
    return null;
  }
}
